package pl.net.szymon.lightexpertsystem.sample.javafx;

/**
 * Created by Szymon on 2016-01-30.
 */
public class KnowledgeBase {
	private int id;
	private String name;
	private String description;
	private User owner;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}
}
