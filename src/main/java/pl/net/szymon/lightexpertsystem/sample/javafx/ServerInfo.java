package pl.net.szymon.lightexpertsystem.sample.javafx;

/**
 * Created by Szymon on 2016-01-30.
 */
public class ServerInfo {
	public static final String CLIENT_ID= "kbExplorer";
	public static final String CLIENT_SECRET= "kbExplorerAppSecret";

	public static final String SERVER_URL= "http://les.szymon.net.pl";
	public static final String AUTHORIZE_PATH= "/oauth/authorize";
	public static final String REDIRECT_URL= "http://les.szymon.net.pl/redirect_receiver";
	public static final String TOKEN_PATH= "/oauth/token";

}
