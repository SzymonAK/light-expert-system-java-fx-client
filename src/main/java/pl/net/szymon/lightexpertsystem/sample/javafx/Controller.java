package pl.net.szymon.lightexpertsystem.sample.javafx;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.StringConverter;
import org.apache.commons.codec.binary.Base64;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

public class Controller {

	private Token token;
	private ObservableList<KnowledgeBaseView> listOfBases = FXCollections.observableArrayList();

	@FXML
	private Label refreshTokenLbl;


	@FXML
	private Label loginLbl;

	@FXML
	private Label nameLbl;

	@FXML
	private TableView<KnowledgeBaseView> kbTable;
	@FXML
	private TableColumn<KnowledgeBaseView, Integer> columnId;
	@FXML
	private TableColumn<KnowledgeBaseView, String> columnName;
	@FXML
	private TableColumn<KnowledgeBaseView, String> columnDescription;

	@FXML
	private void initialize(){


		columnId.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Integer>(){

			@Override
			public String toString(Integer object) {
				return object.toString();
			}

			@Override
			public Integer fromString(String string) {
				return Integer.parseInt(string);
			}

		}));
		columnId.setCellValueFactory(new PropertyValueFactory<KnowledgeBaseView, Integer>("id"));
		columnName.setCellValueFactory(cell -> cell.getValue().nameProperty());
		columnDescription.setCellValueFactory(cell -> cell.getValue().descriptionProperty());
	}


	@FXML
	private void loginBtnAction(ActionEvent event){

		String code = loginUser();

		if(code.contains("error")) {
			displayError();
			return;
		}

		token = authorizeCode(code);
		refreshTokenLbl.setText(token.getRefresh_token());

		User user = getUserInfo();
		loginLbl.setText(user.getLogin());
		nameLbl.setText(user.getFirstName() + " " + user.getLastName());
	}

	@FXML
	private void getKnowledgeBases(ActionEvent event){
		List<KnowledgeBase> kbs;
		try{
			kbs = getKnowledgeBases();
		}catch (Exception ex){
			if(refreshToken()){
				kbs = getKnowledgeBases();
			}else{
				displayError();
				return;
			}
		}
		if(kbs != null){
			for (KnowledgeBase kb:kbs) {
				listOfBases.add(new KnowledgeBaseView(kb.getId(), kb.getName(), kb.getDescription()));
			}
			kbTable.setItems(listOfBases);
		}
	}

	private String loginUser(){
		Dialog dialog = new Dialog();
		dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
		Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
		closeButton.managedProperty().bind(closeButton.visibleProperty());
		closeButton.setVisible(false);

		WebView webView = new WebView();
		WebEngine engine = webView.getEngine();
		engine.load(ServerInfo.SERVER_URL + ServerInfo.AUTHORIZE_PATH + "?response_type=code&client_id=" + ServerInfo.CLIENT_ID +"&redirect_uri=" + ServerInfo.REDIRECT_URL);
		dialog.setResultConverter(x -> {
			return "error";
		});

		engine.locationProperty().addListener(new ChangeListener<String>() {
			@Override public void changed(ObservableValue<? extends String> observable, String oldLocation, String newLocation) {
				if (newLocation != null && newLocation.startsWith(ServerInfo.REDIRECT_URL)){
					dialog.setResultConverter(x -> {
						return newLocation.substring(newLocation.indexOf("code=") + 5);
					});

					dialog.close();
				}
			}
		});

		dialog.getDialogPane().setContent(webView);

		Optional<String> newLocation = dialog.showAndWait();

		return newLocation.orElse("error");
	}

	private void displayError(){
		Alert alert = new Alert(Alert.AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Niestety");
		alert.setContentText("Nastąpił error i nie dostałem kodu autoryzacji");
		alert.showAndWait();
	}

	private Token authorizeCode(String code){
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter mapper = new MappingJackson2HttpMessageConverter();
		mapper.setObjectMapper(new ObjectMapper());
		restTemplate.getMessageConverters().add(mapper);

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		byte[] encodedBytes = Base64.encodeBase64((ServerInfo.CLIENT_ID + ":" + ServerInfo.CLIENT_SECRET).getBytes());
		headers.set("Authorization", "Basic " + new String(encodedBytes));

		HttpEntity<?> entity = new HttpEntity<>(headers);


		ResponseEntity<Token> token = restTemplate.exchange(ServerInfo.SERVER_URL + ServerInfo.TOKEN_PATH +
				"?grant_type=authorization_code&code=" + code + "&redirect_uri=" + ServerInfo.REDIRECT_URL
				,HttpMethod.POST, entity,Token.class);

		return token.getBody();
	}

	private boolean refreshToken(){
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter mapper = new MappingJackson2HttpMessageConverter();
		mapper.setObjectMapper(new ObjectMapper());
		restTemplate.getMessageConverters().add(mapper);

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		byte[] encodedBytes = Base64.encodeBase64((ServerInfo.CLIENT_ID + ":" + ServerInfo.CLIENT_SECRET).getBytes());
		headers.set("Authorization", "Basic " + new String(encodedBytes));

		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {
			ResponseEntity<Token> newToken = restTemplate.exchange(ServerInfo.SERVER_URL + ServerInfo.TOKEN_PATH +
							"?grant_type=refresh_token&refresh_token=" + token.getRefresh_token()
					, HttpMethod.POST, entity, Token.class);
			token = newToken.getBody();
		}catch (Exception ex){
			return false;
		}

		return true;
	}

	private User getUserInfo() {
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter mapper = new MappingJackson2HttpMessageConverter();
		mapper.setObjectMapper(new ObjectMapper());
		restTemplate.getMessageConverters().add(mapper);

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Authorization", "Bearer " + token.getAccess_token());

		HttpEntity<?> entity = new HttpEntity<>(headers);


		ResponseEntity<User> user = restTemplate.exchange(ServerInfo.SERVER_URL + "/api/account", HttpMethod.GET, entity,User.class);

		return user.getBody();
	}

	private List<KnowledgeBase> getKnowledgeBases() {
		RestTemplate restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter mapper = new MappingJackson2HttpMessageConverter();
		mapper.setObjectMapper(new ObjectMapper());
		restTemplate.getMessageConverters().add(mapper);

		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.set("Authorization", "Bearer " + token.getAccess_token());

		HttpEntity<?> entity = new HttpEntity<>(headers);


		ResponseEntity<List<KnowledgeBase>> kbs = restTemplate.exchange(ServerInfo.SERVER_URL + "/api/knowledgeBases",
				HttpMethod.GET, entity,new ParameterizedTypeReference<List<KnowledgeBase>>() {});

		return kbs.getBody();
	}
}
